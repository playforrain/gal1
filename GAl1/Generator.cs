﻿using System;

namespace GAl1
{
    class Generator
    {
        readonly Random _random = new Random();
        readonly object syncLock = new object();

        public int Next(int min, int max)
        {
            lock (syncLock)
            { 
                return _random.Next(min, max);
            }
        }

        public int Next()
        {
            lock (syncLock)
            { 
                return _random.Next();
            }
        }

        public bool GetProbability(float probability)
        {
            if (probability <= 0)
                throw new ArgumentException("percentage");

            lock (syncLock)
            { 
                return _random.NextDouble() < probability;
            }
        }
    }
}
