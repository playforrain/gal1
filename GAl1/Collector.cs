﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace GAl1
{
    class Collector
    {
        const string Where = "data.txt";

        public void Collect(IReadOnlyCollection<Chromosome> population)
        {
            var line = string.Join(" ", population.Select(entry => $"{entry.XValue}=>{entry.YValue}"))
                + Environment.NewLine;

            File.AppendAllText(Where, line);            
        }
    }
}
