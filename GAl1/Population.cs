﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace GAl1
{
    class Population
    {
        readonly Generator _generator;

        readonly Configuration _configuration;

        readonly Collector _collector = new Collector();

        public float YAverageValue { get; private set; }

        public float YMaxValue { get; private set; }

        List<Chromosome> population;

        readonly List<Chromosome> selectedParents = new List<Chromosome>();

        readonly List<Tuple<Chromosome, Chromosome>> marriedParents = new List<Tuple<Chromosome, Chromosome>>();


        public Population(Generator generator) => _generator = generator;

        public Population(Generator generator, Configuration configuration) : this(generator)
        {
            _configuration = configuration;

            //ChromosomesNumber = _configuration.ChromosomesNumber;

            population = new List<Chromosome>();
            /*
             * инициализируем хромосомы случайными значениями. при выдаче рандомом уже
             * имеющегося или при выходе значения за пределы интервала 
             * декрементируем счетчик и генерерим рандом снова
             */

            for (int i = 0; i < _configuration.ChromosomesNumber; i++)
            {
                var candidate = new Chromosome(_generator, _configuration.FitnessFunc, _configuration.Scope,
                    _configuration.Precision);

                while (population.FirstOrDefault(_ => _?.YValue == candidate?.YValue) != null
                     || !candidate.InScope)
                    candidate = new Chromosome(_generator, _configuration.FitnessFunc, _configuration.Scope,
                    _configuration.Precision);


                population.Add(candidate);                
            }            

            Console.WriteLine("=======INIT======");

            foreach (var item in population)
            {
                Console.Write(item.YValue + " ");
            }

            Console.WriteLine();
            Console.WriteLine("=============");


            float accum = 0;

            var minYValue = population.Min(__ => __.YValue);

            for (int i = 0; i < _configuration.ChromosomesNumber; i++)
            {
                //population[i].SetPValue(population.Sum(_ => Math.Abs(_.YValue - (-1))));
                population[i].SetPValue(minYValue, population.Sum(_ => Math.Abs(_.YValue - minYValue)));
                population[i].SetMValue(_configuration.ChromosomesNumber);
                accum += population[i].YValue;               
            }

            YMaxValue = population.Max(_ => _.YValue);

            YAverageValue = accum / _configuration.ChromosomesNumber;

            _collector.Collect(population);
        }

        public Population(Generator generator, Configuration configuration, Chromosome[] genes)
            : this(generator)
        {
            _configuration = configuration;

            population = genes.ToList();            

            float accum = 0;

            var minYValue = population.Min(__ => __.YValue);

            for (int i = 0; i < _configuration.ChromosomesNumber; i++)
            {
                /*
                 * для нахождения вероятности берем сумму абсолютных значений фитнес функции.
                 * при неизвестном минимуме функции за нижнюю границу берем минимальное
                 * значение фитнес функции в популяции
                 */
                population[i].SetPValue(minYValue, population.Sum(_ => Math.Abs(_.YValue - minYValue)));
                population[i].SetMValue(_configuration.ChromosomesNumber);
                accum += population[i].YValue;                
            }

            YMaxValue = population.Max(_ => _.YValue);

            YAverageValue = accum / _configuration.ChromosomesNumber;            
        }
        
        public Population Reproduction()
        {

            for (int i = 0; i < _configuration.ChromosomesNumber; i++)
            {
                var count = (int)Math.Round(population[i].MValue);
                for (int j = 0; j < count; j++)
                    selectedParents.Add(population[i]);                                  
            }

            return this;
        }

        public Population SelectParents()
        {
            //список отобранных для кроссинговера особей не пустой
            //и в нем содержиться более одной уникальной особи
            var checkGenes = selectedParents?.FirstOrDefault()?.Genes;
            if (checkGenes == null)
                throw new Exception("no future  parents");

            if (selectedParents.All(_ => Enumerable.SequenceEqual(_.Genes, checkGenes)))
                throw new Exception("only one parent in set");            

            do
            {
                var fatherIndex = _generator.Next(0, selectedParents.Count);
                var father = selectedParents.ToList()[fatherIndex];

                var motherIndex = _generator.Next(0, selectedParents.Count);
                var mother = selectedParents.ToList()[motherIndex];

                while (fatherIndex == motherIndex || Enumerable.SequenceEqual(father.Genes, mother.Genes))
                {
                    motherIndex = _generator.Next(0, selectedParents.Count);
                    mother = selectedParents.ToList()[motherIndex];
                }
                    
                marriedParents.Add(Tuple.Create(father, mother));
            } while (marriedParents.Count < _configuration.ChromosomesNumber / 2);                      

            return this;
        }


        public Population Crossingover()
        {                                   
            foreach (var entry in marriedParents)
            {
                /*
                 * если true, событие произошло и делаем кроссинговер,
                 * если false, кроссинговер не происходит, в след. поколение идут сами родители
                 */                                             
                if (_generator.GetProbability(_configuration.CrossingoverPropability))
                {
                    // рандом исключает нулевой и последний индекс, иначе кросс теряет смысл
                    var crossPoint = _generator.Next(1, marriedParents.First().Item1.Genes.Length - 2);

                    var newborn1Genes = new bool[entry.Item1.Genes.Length];

                    var newborn2Genes = new bool[entry.Item2.Genes.Length];
                   
                    Array.Copy(entry.Item1.Genes, 0, newborn1Genes, 0, crossPoint);

                    Array.Copy(entry.Item2.Genes, crossPoint, newborn1Genes, crossPoint, newborn2Genes.Length - crossPoint);

                    Array.Copy(entry.Item2.Genes, 0, newborn2Genes, 0, crossPoint);

                    Array.Copy(entry.Item1.Genes, crossPoint, newborn2Genes, crossPoint, newborn1Genes.Length - crossPoint);

                    ////первый ребенок после скрещивания
                    var newborn1 = new Chromosome(_generator, newborn1Genes,
                        _configuration.FitnessFunc, _configuration.Scope);
                    ////второй ребенок после скрещивания
                    var newborn2 = new Chromosome(_generator, newborn2Genes,
                        _configuration.FitnessFunc, _configuration.Scope);

                    population.Add(newborn1);
                    population.Add(newborn2);                   
                }                                            
            }


            return this;
        }        

        public Population Mutation()
        {
            if (_generator.GetProbability(_configuration.MutationPropability))
            {
                var mutantIndex = _generator.Next(0, population.Count - 1);
                var mutantGenes = population[mutantIndex].Genes;
                var mutantGeneIndex = _generator.Next(0, mutantGenes.Length - 1);
                mutantGenes[mutantGeneIndex] = !mutantGenes[mutantGeneIndex];
                var mutant = new Chromosome(_generator, mutantGenes, _configuration.FitnessFunc, _configuration.Scope);
                population[mutantIndex] = mutant;
            }      
            
            return this;
        }        
        
        public Chromosome[] Reduction()
        {
            population.RemoveAll(_ => !_.InScope);

            var goodByeCount = population.Count - _configuration.ChromosomesNumber;

            population = population.OrderByDescending(_ => _.YValue).ToList();

            population.RemoveRange(population.Count - 1 - goodByeCount, goodByeCount);

            _collector.Collect(population);

            return population.ToArray();
        }


    }
}
