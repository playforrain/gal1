﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace GAl1
{
    class Program
    {        
        static readonly Generator _generator = new Generator();

        static void Main(string[] args)
        {
            //test
            //var configuration = new Configuration(100, 0.5f, 0.01f, x => (float)Math.Pow(x, 2),
            //    new[] { ((0f, 31f), (false, false)) }, 3);

            //5
            var configuration = new Configuration(50, 0.5f, 0.01f, x => (float)Math.Cos(2 * x) / (float)Math.Pow(x, 2),
                new[] { ((-20f, -2.3f), (false, false)) }, 3);

            //7
            //var configuration = new Configuration(50, 0.5f, 0.01f, x => (float)Math.Log(x) * (float)Math.Cos(3 * x - 15),
            //    new[] { ((1f, 10f), (false, false)) }, 3);

            //8
            //var configuration = new Configuration(100, 0.5f, 0.01f, x => (float)Math.Cos(3 * x - 15) / Math.Abs(x),
            //    new[] { ((-10f, -0.3f), (false, true)), ((0.3f, 10f), (true, false)) }, 3);

            var population = new Population(_generator, configuration);

            var genes = population
                .Reproduction()
                .SelectParents()
                .Crossingover()
                .Mutation()
                .Reduction();

            Console.WriteLine();
            Console.WriteLine($"====GENERATION 1=====");

            foreach (var item in genes)
                Console.Write($"{item.YValue}({item.XValue}) ");

            Console.WriteLine();

            var counter = 0;

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine($"====GENERATION {++counter}===FF={population.YAverageValue}==");

                population = new Population(_generator, configuration, genes);

                genes = population
                    .Reproduction()
                    .SelectParents()
                    .Crossingover()
                    .Mutation()
                    .Reduction();

                foreach (var item in genes)
                    Console.Write($"{item.YValue}({item.XValue}) ");

                Console.WriteLine();
                Console.WriteLine();

                var key = Console.ReadKey(true);

                if (key.KeyChar == 'q')
                    break;
            }

            Console.WriteLine();

            Console.WriteLine("Нажмите и завершите...");

            Console.ReadKey();
        }        
    }
}
