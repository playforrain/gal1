﻿using System;
using System.Diagnostics;
using System.Linq;

namespace GAl1
{
    [DebuggerDisplay("Y = {YValue}")]
    class Chromosome
    {
        readonly Generator _generator;
        readonly ValueTuple<ValueTuple<float, float>, ValueTuple<bool, bool>>[] _scope;
        const int Base = 2;

        public Chromosome(Generator generator, Func<float, float> fitnessFunc,
           ValueTuple<ValueTuple<float, float>, ValueTuple<bool, bool>>[] scope, int precision) 
        {
            _generator = generator;
            FitnessFunc = fitnessFunc;
            _scope = scope;

            InitGenes(precision);

            for (int i = 0; i < Genes.Length; i++)
                //Genes[i] = Convert.ToBoolean(rand.Next(0,2));

                Genes[i] = Convert.ToBoolean(_generator.Next() % 2);

            SetXValue();
            YValue = (float)Math.Round(FitnessFunc.Invoke(XValue), 4);           
        }

        public Chromosome(Generator generator, bool[] genes, Func<float, float> fitnessFunc,
            ValueTuple<ValueTuple<float, float>, ValueTuple<bool, bool>>[] scope) 
        {
            _generator = generator;
            FitnessFunc = fitnessFunc;
            _scope = scope;

            Genes = genes;
            SetXValue();
            YValue = (float)Math.Round(FitnessFunc.Invoke(XValue), 4);
        }

        public Guid Id { get; private set; } = Guid.NewGuid();

        public bool[] Genes { get; private set; } 
        

        public float XValue { get; private set; }

        public float YValue { get; private set; }

        public float PValue { get; private set; }

        public float MValue { get; private set; }

        public bool InScope { get; private set; }

        Func<float, float> FitnessFunc;
        
        public void SetPValue(float populationMinYValue, float totalYValues) => PValue = 
            Math.Abs(YValue - populationMinYValue) / totalYValues;

        public void SetMValue(int nValue) => MValue = PValue * nValue;

        //float scope var
        void SetXValue()
        {
            var reversedGenes = Genes.Reverse().ToArray();

            XValue = 0.0f;

            for (int i = 0; i < reversedGenes.Length; i++)
                XValue += (float)(Convert.ToDouble(reversedGenes[i]) * Math.Pow(2.0, i));

            var scope = _scope.Last().Item1.Item2 - _scope.First().Item1.Item1;

            //var scope = _scope.Item2 - _scope.Item1;

            //XValue = _scope.Item1 + XValue * scope
            //    / ((float)(Math.Pow(2, Genes.Length)) - 1);

            //InScope = XValue <= _scope.Item2 && XValue >= _scope.Item1;

            XValue = (float)Math.Round(_scope.First().Item1.Item1 + XValue * scope
                / ((float)(Math.Pow(2, Genes.Length)) - 1), 4);

            //InScope = XValue <= _scope.Item2 && XValue >= _scope.Item1;
            foreach (var entry in _scope)
            {
                if (!entry.Item2.Item1 && !entry.Item2.Item2)
                {
                    if (entry.Item1.Item1 <= XValue && entry.Item1.Item2 >= XValue)
                        InScope = true;
                }                    
                else if (!entry.Item2.Item1 && entry.Item2.Item2)
                {
                    if (entry.Item1.Item1 <= XValue && entry.Item1.Item2 > XValue)
                        InScope = true;
                }                    
                else if (entry.Item2.Item1 && !entry.Item2.Item2)
                {
                    if (entry.Item1.Item1 < XValue && entry.Item1.Item2 >= XValue)
                        InScope = true;
                }
                    
            }
        }

        void InitGenes(int precision)
        {            
            if (_scope == null || _scope.Length == 0 || 
                _scope.First().Item1.Item1 >= _scope.Last().Item1.Item2)
                throw new ArgumentException("scope");

            var delta = _scope.Last().Item1.Item2 - _scope.First().Item1.Item1;

            var requiredCodingVectorLength = delta * Math.Pow(10, precision);
            var currentCodingVectorLength = 0.0;
            var powGrowingCounter = 0;

            while (currentCodingVectorLength < requiredCodingVectorLength)
                currentCodingVectorLength = Math.Pow(Base, ++powGrowingCounter);

            Genes = new bool[powGrowingCounter];
        }
    }
}
