﻿using System;

namespace GAl1
{
    class Configuration
    {
        public Configuration(int chromosomesNumber, float crossingoverPropability,
            float mutationPropability, Func<float, float> fitnessFunc,
            ValueTuple<ValueTuple<float, float>, ValueTuple<bool, bool>>[] scope,
            int precision)
        {
            ChromosomesNumber = chromosomesNumber;
            CrossingoverPropability = crossingoverPropability;
            MutationPropability = mutationPropability;
            FitnessFunc = fitnessFunc;
            Scope = scope;
            Precision = precision;
        }

        public int ChromosomesNumber { get; private set; }

        public float CrossingoverPropability { get; private set; }

        public float MutationPropability { get; private set; }

        public Func<float, float> FitnessFunc { get; private set; }

        //public ValueTuple<float, float> Scope { get; private set; }
        public ValueTuple<ValueTuple<float, float>, ValueTuple<bool, bool>>[] Scope { get; private set; }

        public int Precision { get; private set; }

    }
}
